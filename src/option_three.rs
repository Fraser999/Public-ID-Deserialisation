/// Implement custom `Serialize` and `Deserialize` and actually just deal with a tuple of the
/// required fields - really using a new private type to do the work.

use super::*;
use serde::de::{Deserialize, Deserializer};
use serde::ser::{Serialize, Serializer};

#[derive(Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    name: XorName,
}

impl Serialize for PublicId {
    fn serialize<S: Serializer>(&self, serialiser: S) -> Result<S::Ok, S::Error> {
        (&self.public_sign_key, &self.public_encrypt_key).serialize(serialiser)
    }
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        let res: (PublicSigningKey, PublicEncryptingKey) = Deserialize::deserialize(deserialiser)?;
        Ok(PublicId::new(res.0, res.1))
    }
}

impl PublicId {
    pub fn new(sign_key: PublicSigningKey, encrypt_key: PublicEncryptingKey) -> PublicId {
        let name = name_from_key(&sign_key);
        PublicId { public_sign_key: sign_key, public_encrypt_key: encrypt_key, name: name }
    }
}
