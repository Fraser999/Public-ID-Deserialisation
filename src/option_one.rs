/// Derive `Serialize`, and implement full functionality in `Deserialize` (i.e. can parse from
/// bincode-encoded or json-encoded).

use super::*;
use serde::de::{self, Deserialize, Deserializer, MapAccess, SeqAccess, Visitor};
use std::fmt;

#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        #[allow(bad_style)]
        enum Field {
            Public_Sign_Key,
            Public_Encrypt_Key,
        };

        struct PublicIdVisitor;

        impl<'de> Visitor<'de> for PublicIdVisitor {
            type Value = PublicId;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct PublicId")
            }

            fn visit_seq<V: SeqAccess<'de>>(self, mut seq: V) -> Result<PublicId, V::Error> {
                let public_sign_key: PublicSigningKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(0, &self));
                    }
                };
                let public_encrypt_key: PublicEncryptingKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(1, &self));
                    }
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }

            fn visit_map<V: MapAccess<'de>>(self, mut map: V) -> Result<PublicId, V::Error> {
                let mut public_sign_key = None;
                let mut public_encrypt_key = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Public_Sign_Key => {
                            if public_sign_key.is_some() {
                                return Err(de::Error::duplicate_field("public_sign_key"));
                            }
                            public_sign_key = Some(map.next_value()?);
                        }
                        Field::Public_Encrypt_Key => {
                            if public_encrypt_key.is_some() {
                                return Err(de::Error::duplicate_field("public_encrypt_key"));
                            }
                            public_encrypt_key = Some(map.next_value()?);
                        }
                    }
                }
                let public_sign_key = match public_sign_key {
                    Some(public_sign_key) => public_sign_key,
                    None => return Err(de::Error::missing_field("public_sign_key")),
                };
                let public_encrypt_key = match public_encrypt_key {
                    Some(public_encrypt_key) => public_encrypt_key,
                    None => return Err(de::Error::missing_field("public_encrypt_key")),
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }
        }

        const FIELDS: &'static [&'static str] = &["public_sign_key", "public_encrypt_key"];
        deserialiser.deserialize_struct("PublicId", FIELDS, PublicIdVisitor)
    }
}

impl PublicId {
    pub fn new(sign_key: PublicSigningKey, encrypt_key: PublicEncryptingKey) -> PublicId {
        let name = name_from_key(&sign_key);
        PublicId { public_sign_key: sign_key, public_encrypt_key: encrypt_key, name: name }
    }
}
