/// Derive `Serialize`, and implement custom `Deserialize` hoping that parsing to a tuple of the
/// required fields will continue to work in the future.

use super::*;
use serde::de::{Deserialize, Deserializer};

#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        let res: (PublicSigningKey, PublicEncryptingKey) = Deserialize::deserialize(deserialiser)?;
        Ok(PublicId::new(res.0, res.1))
    }
}

impl PublicId {
    pub fn new(sign_key: PublicSigningKey, encrypt_key: PublicEncryptingKey) -> PublicId {
        let name = name_from_key(&sign_key);
        PublicId { public_sign_key: sign_key, public_encrypt_key: encrypt_key, name: name }
    }
}
