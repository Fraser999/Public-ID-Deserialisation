/// Derive `Serialize`, and implement only sequence functionality in `Deserialize` (i.e. can only
/// parse from bincode-encoded).

use super::*;
use serde::de::{self, Deserialize, Deserializer, SeqAccess, Visitor};
use std::fmt;

#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        struct PublicIdVisitor;

        impl<'de> Visitor<'de> for PublicIdVisitor {
            type Value = PublicId;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct PublicId")
            }

            fn visit_seq<V: SeqAccess<'de>>(self, mut seq: V) -> Result<PublicId, V::Error> {
                let public_sign_key: PublicSigningKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(0, &self));
                    }
                };
                let public_encrypt_key: PublicEncryptingKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(1, &self));
                    }
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }
        }

        const FIELDS: &'static [&'static str] = &["public_sign_key", "public_encrypt_key"];
        deserialiser.deserialize_struct("PublicId", FIELDS, PublicIdVisitor)
    }
}

impl PublicId {
    pub fn new(sign_key: PublicSigningKey, encrypt_key: PublicEncryptingKey) -> PublicId {
        let name = name_from_key(&sign_key);
        PublicId { public_sign_key: sign_key, public_encrypt_key: encrypt_key, name: name }
    }
}
