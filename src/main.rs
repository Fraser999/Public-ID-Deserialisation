//! Examples of methods to deserialise `PublicId`s.

#![forbid(warnings)]
#![warn(missing_copy_implementations, trivial_casts, trivial_numeric_casts, unsafe_code,
        unused_extern_crates, unused_import_braces, unused_qualifications, unused_results,
        variant_size_differences)]
#![allow(missing_docs)]
#![cfg_attr(feature="cargo-clippy", deny(clippy))]

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate unwrap;

extern crate serde;
extern crate bincode;

mod option_one;
mod option_two;
mod option_three;
mod option_four;

use bincode::{Infinite, deserialize, serialize};

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy, Eq, PartialEq)]
pub struct PublicSigningKey(pub [u8; 5]);

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy, Eq, PartialEq)]
pub struct PublicEncryptingKey(pub [u8; 3]);

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy, Eq, PartialEq)]
pub struct XorName(pub [u8; 5]);

fn name_from_key(key: &PublicSigningKey) -> XorName { XorName(key.0) }

fn main() {
    let id1 = option_one::PublicId::new(PublicSigningKey([1; 5]), PublicEncryptingKey([2; 3]));
    let serialised1 = unwrap!(serialize(&id1, Infinite));
    let deserialised1: option_one::PublicId = unwrap!(deserialize(&serialised1));
    println!("deserialised 1 = {:?}", deserialised1);
    assert_eq!(deserialised1, id1);

    let id2 = option_two::PublicId::new(PublicSigningKey([3; 5]), PublicEncryptingKey([4; 3]));
    let serialised2 = unwrap!(serialize(&id2, Infinite));
    let deserialised2: option_two::PublicId = unwrap!(deserialize(&serialised2));
    println!("deserialised 2 = {:?}", deserialised2);
    assert_eq!(deserialised2, id2);

    let id3 = option_three::PublicId::new(PublicSigningKey([5; 5]), PublicEncryptingKey([6; 3]));
    let serialised3 = unwrap!(serialize(&id3, Infinite));
    let deserialised3: option_three::PublicId = unwrap!(deserialize(&serialised3));
    println!("deserialised 3 = {:?}", deserialised3);
    assert_eq!(deserialised3, id3);

    let id4 = option_four::PublicId::new(PublicSigningKey([7; 5]), PublicEncryptingKey([8; 3]));
    let serialised4 = unwrap!(serialize(&id4, Infinite));
    let deserialised4: option_four::PublicId = unwrap!(deserialize(&serialised4));
    println!("deserialised 4 = {:?}", deserialised4);
    assert_eq!(deserialised4, id4);
}
