# Public-ID-Deserialisation

Examples of methods to deserialise PublicIds.

* [Option 1](#option-1) Derive `Serialize`, and implement full functionality in `Deserialize` (i.e. can parse from bincode-encoded or json-encoded).
* [Option 2](#option-2) Derive `Serialize`, and implement only sequence functionality in `Deserialize` (i.e. can only parse from bincode-encoded).
* [Option 3](#option-3) Implement custom `Serialize` and `Deserialize` and actually just deal with a tuple of the required fields - really using a new private type to do the work.
* [Option 4](#option-4) Derive `Serialize`, and implement custom `Deserialize` hoping that parsing to a tuple of the required fields will continue to work in the future.

## Option 1

Derive `Serialize`, and implement full functionality in `Deserialize` (i.e. can parse from bincode-encoded or json-encoded).

[(Top of page)](#public-id-deserialisation)

```rust
#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        #[allow(bad_style)]
        enum Field {
            Public_Sign_Key,
            Public_Encrypt_Key,
        };

        struct PublicIdVisitor;

        impl<'de> Visitor<'de> for PublicIdVisitor {
            type Value = PublicId;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct PublicId")
            }

            fn visit_seq<V: SeqAccess<'de>>(self, mut seq: V) -> Result<PublicId, V::Error> {
                let public_sign_key: PublicSigningKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(0, &self));
                    }
                };
                let public_encrypt_key: PublicEncryptingKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(1, &self));
                    }
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }

            fn visit_map<V: MapAccess<'de>>(self, mut map: V) -> Result<PublicId, V::Error> {
                let mut public_sign_key = None;
                let mut public_encrypt_key = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Public_Sign_Key => {
                            if public_sign_key.is_some() {
                                return Err(de::Error::duplicate_field("public_sign_key"));
                            }
                            public_sign_key = Some(map.next_value()?);
                        }
                        Field::Public_Encrypt_Key => {
                            if public_encrypt_key.is_some() {
                                return Err(de::Error::duplicate_field("public_encrypt_key"));
                            }
                            public_encrypt_key = Some(map.next_value()?);
                        }
                    }
                }
                let public_sign_key = match public_sign_key {
                    Some(public_sign_key) => public_sign_key,
                    None => return Err(de::Error::missing_field("public_sign_key")),
                };
                let public_encrypt_key = match public_encrypt_key {
                    Some(public_encrypt_key) => public_encrypt_key,
                    None => return Err(de::Error::missing_field("public_encrypt_key")),
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }
        }

        const FIELDS: &'static [&'static str] = &["public_sign_key", "public_encrypt_key"];
        deserialiser.deserialize_struct("PublicId", FIELDS, PublicIdVisitor)
    }
}
```

## Option 2

Derive `Serialize`, and implement only sequence functionality in `Deserialize` (i.e. can only parse from bincode-encoded).

[(Top of page)](#public-id-deserialisation)

```rust
#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        struct PublicIdVisitor;

        impl<'de> Visitor<'de> for PublicIdVisitor {
            type Value = PublicId;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct PublicId")
            }

            fn visit_seq<V: SeqAccess<'de>>(self, mut seq: V) -> Result<PublicId, V::Error> {
                let public_sign_key: PublicSigningKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(0, &self));
                    }
                };
                let public_encrypt_key: PublicEncryptingKey = match seq.next_element()? {
                    Some(value) => value,
                    None => {
                        return Err(de::Error::invalid_length(1, &self));
                    }
                };
                Ok(PublicId::new(public_sign_key, public_encrypt_key))
            }
        }

        const FIELDS: &'static [&'static str] = &["public_sign_key", "public_encrypt_key"];
        deserialiser.deserialize_struct("PublicId", FIELDS, PublicIdVisitor)
    }
}
```

## Option 3

Implement custom `Serialize` and `Deserialize` and actually just deal with a tuple of the required fields - really using a new private type to do the work.

[(Top of page)](#public-id-deserialisation)

```rust
#[derive(Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    name: XorName,
}

impl Serialize for PublicId {
    fn serialize<S: Serializer>(&self, serialiser: S) -> Result<S::Ok, S::Error> {
        (&self.public_sign_key, &self.public_encrypt_key).serialize(serialiser)
    }
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        let res: (PublicSigningKey, PublicEncryptingKey) = Deserialize::deserialize(deserialiser)?;
        Ok(PublicId::new(res.0, res.1))
    }
}
```

## Option 4

Derive `Serialize`, and implement custom `Deserialize` hoping that parsing to a tuple of the required fields will continue to work in the future.

[(Top of page)](#public-id-deserialisation)

```rust
#[derive(Serialize, Debug, Default, PartialEq, Eq)]
pub struct PublicId {
    public_sign_key: PublicSigningKey,
    public_encrypt_key: PublicEncryptingKey,
    #[serde(skip_serializing)]
    name: XorName,
}

impl<'de> Deserialize<'de> for PublicId {
    fn deserialize<D: Deserializer<'de>>(deserialiser: D) -> Result<Self, D::Error> {
        let res: (PublicSigningKey, PublicEncryptingKey) = Deserialize::deserialize(deserialiser)?;
        Ok(PublicId::new(res.0, res.1))
    }
}
```
